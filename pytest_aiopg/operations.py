from functools import partial
from operator import itemgetter, methodcaller
from typing import Tuple

from pytest import fixture
from sqlalchemy import insert, select
from sqlalchemy.ext.declarative import DeclarativeMeta

__all__ = [
    'aiopg_sa_model_fetchall',
    'aiopg_sa_model_insert',
]


@fixture()
def aiopg_sa_model_fetchall(aiopg_sa_engine):

    async def fetchall(model_cls: DeclarativeMeta,
                       *omit_keys: str) -> Tuple:
        async with aiopg_sa_engine.acquire() as conn:
            result = await conn.execute(select([model_cls]))
            keys = tuple(result.keys())
            data = await result.fetchall()

        if omit_keys:
            omit_keys = frozenset(omit_keys)
            keys = tuple(filter(lambda x: x not in omit_keys, keys))

        return tuple(map(
            dict,
            map(
                partial(zip, keys),
                map(
                    itemgetter(*keys), data
                )
            )
        ))

    return fetchall


@fixture()
def aiopg_sa_model_insert(aiopg_sa_engine):

    async def insert_(model_cls: DeclarativeMeta,
                      *values: dict,
                      returning: Tuple = (),
                      fetch_method: str = 'fetchall'):
        async with aiopg_sa_engine.acquire() as conn:
            result_proxy = await conn.execute(
                insert(model_cls).values(values).returning(*returning)
            )

            if result_proxy.closed:
                return

            return await methodcaller(fetch_method)(result_proxy)

    return insert_
