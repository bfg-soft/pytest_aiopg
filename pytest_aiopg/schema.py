from contextlib import closing
from io import StringIO
from logging import getLogger
from operator import attrgetter

from aiopg.sa import Engine
from pytest import fixture
from sqlalchemy import MetaData, create_engine, text
from sqlalchemy.sql.base import Executable
from sqlalchemy.sql.elements import TextClause

__all__ = [
    'aiopg_sa_schema',
    'aiopg_sa_metadata',
    'aiopg_sa_create_tables',
    'aiopg_sa_drop_tables',
    'aiopg_sa_truncate_tables',
    'aiopg_sa_clear_tables',
]


_logger = getLogger(__name__)


# based on https://docs.sqlalchemy.org/en/latest/faq/metadata_schema.html#how
# -can-i-get-the-create-table-drop-table-output-as-a-string
def _get_sql_statement(func: callable) -> TextClause:
    with closing(StringIO()) as buffer:
        # noinspection PyUnusedLocal
        def dump(sql, *multiparams, **params):
            buffer.write('{};\n'.format(
                sql.compile(dialect=engine.dialect)
            ))

        engine = create_engine('postgres://',
                               strategy='mock',
                               executor=dump)
        # Mock engine can not check if table exists in database.
        func(engine, checkfirst=False)
        return text(buffer.getvalue())


async def _execute(engine: Engine, executable: Executable) -> None:
    async with engine.acquire() as conn:
        async with conn.begin():
            await conn.execute(executable)


class _SchemaCreated(object):
    def __init__(self):
        self.__created = False

    @property
    def created(self):
        return self.__created

    @property
    def dropped(self):
        return not self.__created

    def set_created(self):
        self.__created = True

    def unset_created(self):
        self.__created = False

    set_dropped = unset_created


@fixture(scope='session')
def aiopg_sa_schema() -> _SchemaCreated:
    return _SchemaCreated()


@fixture(scope='session')
def aiopg_sa_metadata() -> MetaData:
    raise NotImplemented()


@fixture()
def aiopg_sa_truncate_tables(aiopg_sa_engine, aiopg_sa_metadata):

    async def truncate():
        await _execute(
            aiopg_sa_engine,
            text(
                'TRUNCATE "{}"'.format(
                    '", "'.join(map(
                        attrgetter('name'),
                        reversed(aiopg_sa_metadata.sorted_tables)
                    ))
                )
            )
        )
        _logger.debug('Database has been just cleared.')

    return truncate


@fixture()
def aiopg_sa_create_tables(aiopg_sa_engine, aiopg_sa_metadata, aiopg_sa_schema):

    async def create():

        if aiopg_sa_schema.created:
            _logger.warning('Database schema had been created above.')
            return

        await _execute(
            aiopg_sa_engine,
            _get_sql_statement(aiopg_sa_metadata.create_all)
        )

        aiopg_sa_schema.set_created()
        _logger.debug('Database schema has been created.')

    return create


@fixture()
def aiopg_sa_drop_tables(aiopg_sa_engine, aiopg_sa_metadata, aiopg_sa_schema):

    async def drop():
        await _execute(
            aiopg_sa_engine,
            _get_sql_statement(aiopg_sa_metadata.drop_all)
        )

        aiopg_sa_schema.set_dropped()
        _logger.debug('Database schema has been dropped.')

    return drop


@fixture()
def aiopg_sa_clear_tables(
    aiopg_sa_event_loop,
    aiopg_sa_create_tables,
    aiopg_sa_truncate_tables,
    aiopg_sa_schema,
):
    if aiopg_sa_schema.dropped:
        aiopg_sa_event_loop.run_until_complete(aiopg_sa_create_tables())

    aiopg_sa_event_loop.run_until_complete(aiopg_sa_truncate_tables())
