from asyncio import AbstractEventLoop, get_event_loop
from typing import Awaitable, Callable

from pytest import fixture

__all__ = [
    'aiopg_sa_engine_factory',
    'aiopg_sa_event_loop',
    'aiopg_sa_engine',
]


@fixture()
def aiopg_sa_engine(aiopg_sa_event_loop, aiopg_sa_engine_factory):
    """Синхронная функция-fixture для получения асинхронного движка работы с
    БД postgres aiopg."""
    aiopg_engine = aiopg_sa_event_loop.run_until_complete(
        aiopg_sa_engine_factory()
    )

    try:
        yield aiopg_engine

    finally:
        aiopg_engine.close()
        aiopg_sa_event_loop.run_until_complete(aiopg_engine.wait_closed())


@fixture()
def aiopg_sa_engine_factory() -> Callable[[], Awaitable]:
    raise NotImplemented()


@fixture()
def aiopg_sa_event_loop() -> AbstractEventLoop:
    return get_event_loop()
