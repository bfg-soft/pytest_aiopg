from setuptools import find_packages, setup

_NAME = 'pytest_aiopg'

setup(
    name=_NAME,
    version='0.1.0',
    packages=find_packages(),
    url='https://bitbucket.org/bfg-soft/pytest_aiopg',
    license='MIT',
    author='BFG-Soft LLC',
    author_email='info@bfg-soft.ru',
    description='Pytest plugin for aiopg driver.',
    install_requires=[
        'pytest',
        'sqlalchemy',
        'aiopg',
    ],
    entry_points={
        'pytest11': [
            '{0} = {0}'.format(_NAME)
        ]
    }
)
