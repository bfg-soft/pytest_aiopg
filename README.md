# Pytest plugin for [aiopg](https://github.com/aio-libs/aiopg) sql engine.

Plugin imlements some useful fixtures for tests with aiopg engine.


## Getting started

### Installation

We have no pypi account yet and we have no private pypi repository.
So, you should install this package as usual with `pip` package manager in 
virtualenv or just globally from source:
```bash
pip install git+https://bitbucket.org/bfg-soft/pytest_aiopg.git
```
If you want to install specific version or branch, you should execute:
```bash
pip install git+https://bitbucket.org/bfg-soft/pytest_aiopg.git@v0.1.0
```

*Plugin is available for pytest automatically, because of it\`s entry_point.*


### Content

#### Defined required fixtures in your tests.
For using `pytest_aiopg` plugin, you should overwrite some fixtures:

 * `aiopg_sa_engine_factory`. Fixture, that must return coroutine function, 
 without any required arguments. This coroutine function must return 
 `aiopg.sa.Engine`instance. Becides that, this function can check ability of 
 connection to test database inside itself.
 
 * `aiopg_sa_metadata`. Fixture, that must return `sqlalchemy.MetaData` 
 object for your project\`s sqlalchemy model. It is required only in cases, 
 when schema-based fixtures, like `aiopg_sa_truncate_tables`,
 `aiopg_sa_create_tables`, `aiopg_sa_drop_tables`, `aiopg_sa_clear_tables` 
 will be used in your tests.
 
 * `aiopg_sa_event_loop`. Fixture, that must return current 
 `asyncio.AbstractEventLoop` instance for tests. Optionally.
 
Example:
```python
from asyncio import sleep

from aiopg.sa import create_engine
from pytest import fixture
from psycopg2 import OperationalError

from youproject import BaseSQLModel


@fixture()
def loop():
    # Some fixture, that is defined in yourpackage tests, or provided from 
    # another plugin (like pytest-asyncio or pytest-aiohttp, for example).
    ...


@fixture(scope='session')
def aiopg_sa_metadata():
    return BaseSQLModel.metadata


@fixture()
def aiopg_sa_event_loop(loop):
    return loop
    
    
@fixture()
def database_dsn():
    # Some fixture, that is defined it you package test any way: from some 
    # settings, config file or just hardcoded.
    ...
    

@fixture()
async def aiopg_sa_engine_factory(database_dsn, loop):

    async def factory():
        aiopg_engine = None

        async def connect():
            nonlocal aiopg_engine

            try:
                aiopg_engine = await create_engine(database_dsn, loop=loop)
                return True

            except OperationalError:
                return False
        
        # Waiting for connection.
        while not await connect():
            await sleep(.1)

        return aiopg_engine

    return factory
```


#### Available fixiture

* `aiopg_sa_engine` - returns instance of `aiopg.sa.Engine`.

* `aiopg_sa_schema` - object, that indicates state of database schema during 
tests. It has `created` and `dropped` bool attributes and `set_created` and 
`set_dropped` methods. It changes schema state during calling 
`aiopg_sa_create_tables`, `aiopg_sa_drop_tables` and `aiopg_sa_clear_tables` 
fixtures.

* `aiopg_sa_truncate_tables` - returns coroutine function, that truncates all
 database schema tables with native `TRUNCATE` postgresql syntax.
 
* `aiopg_sa_create_tables` - returns coroutine function, that creates all
 database schema tables.
 
* `aiopg_sa_drop_tables` - returns coroutine function, that drops all
 database schema tables.
 
* `aiopg_sa_clear_tables` - returns coroutine function, that creates all
 database schema tables, if they didn`t created yet and truncate them.
 
* `aiopg_sa_model_fetchall` - return coroutine function, that 
fetches all records of model, as it described in function`s name and accepts:
    * sqlalchemy declarative model class as required positional argument,
    * column names to omit as additional positional arguments. 
    
* `aiopg_sa_model_insert` - return coroutine function, that inserts one 
or mode records of model to database and accepts:    
    * sqlalchemy declarative model class as required positional argument,
    * dicts of values as additional positional arguments,
    * tuple of returning values as keyword argument `returning`
    (by default it is empty),
    * string of fetch method, if returning was defined, 
    as keyword argument `fetch_method` 
    (see https://aiopg.readthedocs.io/en/stable/sa.html#aiopg.sa.ResultProxy).


## License

This project is licensed under the MIT License - see the 
[LICENSE.txt](LICENSE.txt) file for details.
